import React, { Component } from "react";
import {
  CForm,
  CButton,
  CCol,
  CFormGroup,
  CInput,
  CLabel,
  CCard,
  CCardHeader,
  CCardBody,
  CCardFooter,
} from "@coreui/react";
import api from "src/services/api";
import swal from "sweetalert2";

class RequestAssets extends Component {
  constructor(props) {
    super(props);
    this.state = {
      staff_id: "",
      item_id: "",
      purpose: "",
      borrowDate: "",
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleRequest = this.handleRequest.bind(this);
  }

  componentDidMount() {
    this.getStaffId();
  }

  getStaffId() {
    let value = localStorage.getItem("users");
    let json = JSON.parse(value);
    let staffId = json.id;
    this.setState({ staff_id: staffId });
  }

  // getItemId() {
  //   this.setState({ item_id: this.props.location.state.item_id });
  // }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleRequest() {
    var data = {
      staff_id: this.state.staff_id,
      item_id: this.props.item_id,
      purpose: this.state.purpose,
      borrowed_at: this.state.borrowDate,
    };
    // console.log(data);
    api
      .post("/api/logRequest", data)
      .then(() => {
        swal
          .fire({
            title: "Request Sent!",
            text: "Please submit Return Form after use. \n Thank you. ",
            icon: "success",
            confirmButtonColor: "#5bc0de",
          })
          .then(() => {
            window.location.reload();
          });
      })
      .catch((error) => {
        swal.fire({
          title: "Error",
          text: error,
          icon: "error",
        });
      });
  }

  render() {
    return (
      <div>
        <CCard>
          <CCardHeader>
            <h4 className="text-center p-1">Log Request Item</h4>
          </CCardHeader>
          <CCardBody>
            <CForm>
              <CFormGroup row>
                <CCol sm="3" className="m-1">
                  <CLabel htmlFor="purpose-input">Purpose</CLabel>
                </CCol>
                <CCol className="p-1">
                  <CInput
                    type="text"
                    id="purpose-input"
                    name="purpose"
                    placeholder="Please state purpose"
                    onChange={this.handleChange}
                  />
                </CCol>
              </CFormGroup>

              <CFormGroup row>
                <CCol sm="3" className="m-1">
                  <CLabel htmlFor="date-input">Date</CLabel>
                </CCol>
                <CCol className="p-1">
                  <CInput
                    type="date"
                    id="date-input"
                    name="borrowDate"
                    placeholder="date"
                    onChange={this.handleChange}
                  />
                </CCol>
              </CFormGroup>
            </CForm>
          </CCardBody>
          <CCardFooter className="text-center">
            <CButton color="info" onClick={this.handleRequest}>
              Submit
            </CButton>
          </CCardFooter>
        </CCard>
      </div>
    );
  }
}

export default RequestAssets;
