import React from "react";
import { CBreadcrumbRouter } from "@coreui/react";
// routes config
import routes from "../routes";

function Breadcrumbs() {
  return (
    <div>
      <CBreadcrumbRouter
        className="border-0 c-subheader-nav m-0 px-0"
        routes={routes}
      />
    </div>
  );
}
export default Breadcrumbs;
